parseTimeObject.words.en = {
  currently: ['teraz'],
  clockwords: [
    'godzin'
  ],
  numbers: {
    'zero' : 0,
    'jeden' : 1,
    'dwa' : 2,
    'trzy' : 3,
    'kilka' : 3.5,
    'some' : 3.5,
    'cztery' : 4,
    'pięć' : 5,
    'sześć' : 6,
    'siedem' : 7,
    'osiem' : 8,
    'dziewięć' : 9,
    'dziesięć' : 10,
    'jedenaście' : 11,
    'dwanaście' : 12,
    'trzynaście' : 13,
    'czternaście' : 14,
    'piętnaście' : 15,
    'szesnaście' : 16,
    'siedemnaście' : 17,
    'osiemnaście' : 18,
    'dziewiętnaście' : 19,
    'dwadzieścia' : 20,
    'trzydzieści' : 30,
    'czterdzieści' : 40,
    'pięćdziesiąt' : 50,
    'sześćdziesiąt' : 60,
    'siedemdziesiąt' : 70,
    'osiemdziesiąt' : 80,
    'dziewięćdziesiąt' : 90,
    'sto' : 100,
    'tysiąc' : 1000,
    'milion' : 1000000
  },
  unit: {
    'milisekunda' : 1,
    'sekunda' : 1000,
    'minuta' : 60000,
    'godzina' : 3600000,
    'dzień' : 86400000,
    'tydzień' : 604800000,
    'miesiąc' : 2592000000,
    'kwartał' : 7776000000,
    'rok' : 31536000000,
    'dekada' : 315360000000
  },
  countable: {
    'przedwczoraj' : -17280000,
    'wczoraj' : -8640000,
    'dzisiaj' : 1,
    'pojutrze' : 17280000,
    'jutro' : 8640000,
    'za tydzień' : 60480000,
    'w zeszłym tygodniu' : -60480000
  },
  month: {
    'sty' : '01',
    'lut' : '02',
    'mar' : '03',
    'kwi' : '04',
    'maj' : '05',
    'cze' : '06',
    'lip' : '07',
    'sie' : '08',
    'wrz' : '09',
    'paź' : '10',
    'lis' : '11',
    'gru' : '12'
  },
  daytime: {
    'świt': '04:00',
    'rano': '06:00',
    'popołudnie': '15:00',
    'północ': '12:00',
    'południe': '12:00',
    'podwieczór': '17:00',
    'pod-wieczór': '17:00',
    'wieczór': '19:00',
    'zmierzch': '20:00',
    'północ': '24:00',
    'noc': '22:00'
  },
  fillwords: {
    'temu' : '-',
    'za' : '+'
  },
  fillfoo: {
    's' : '',
    '\\-' : '',
    '\\ ' : '',
    '\\.' : ''
  }
};
